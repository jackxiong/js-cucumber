
const {Given, When, Then} = require('cucumber');
const search = require('../support/pages/googlePage');

console.log('test google search')

Given('I am open Google\'s search page', async function () {
  
   await testController.navigateTo(search.google.url());
   console.log(search.google.url())
  });

When('I am typing my search request {string} on Google', async function (string) {
   
    //await this.addScreenshotToReport();
    await testController.typeText(search.google.searchBox(), string);
    console.log("I am typing my search request {string} on Google",string)
    
  });

Then('I press the {string} key on Google', async function (string) {
   
    await testController.pressKey(string);
    console.log("I press the {string} key on Google",string)
    
  });

Then('I should see that the first Google\'s result is {string}', async function (string) {

    console.log("I should see that the first Google\'s result is {string}",string)
    const text = search.google.searchResult().innerText
    await testController.expect(text).contains(string);

  });

 