const {Selector} = require('testcafe')
const XPathSelector = require('../../../xpath/xpath-selector')

exports.google = {

    url: function() {
        return 'https://www.google.com';
    },
    searchBox: function() {
        var input = Selector('.gLFyf').with({boundTestRun: testController});
        return input;//select('.header-search-input');
    },
    searchButton: function() {
        return select('.header-search-input');
    },

    searchResult:function () {

        var firstLink = Selector('#rso').find('a').with({boundTestRun: testController});
        //var firstLink = Selector('alt').with({boundTestRun: testController})
        //('//div[@class="g"]//div//div[@class="rc"]//h3[@class="LC20lb DKV0Md"][contains(text(),"DevExpress")]')
        return firstLink;
    }
};