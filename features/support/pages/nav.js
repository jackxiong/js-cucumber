const {Selector} = require('testcafe')
const XPathSelector = require('../../../xpath/xpath-selector')

exports.nav = {

    url: function() {
        return 'https://www.nab.com.au/';
    },
    Persoal:function(){

        var persoanl = XPathSelector('//a[@class="menu-trigger"][contains(text(),"Personal")]');
        return persoanl

    },

    home_Loan:function(){
        var homeLoan =XPathSelector('/html/body/div[1]/div/div[3]/div/header/div[3]/nav/ul/li[1]/ul/li[5]/div/ul/li[1]/a/span');
        return homeLoan;
    },

    h_l:function(){
        var hl =XPathSelector('//body[@class="js"]/div[@class="outer"]/div[@id="main"]/div[@id="wrapper"]/div[@id="header-container"]/header/div[@id="menu-logo-container"]/nav[@class="hidden-xs primary-nav"]/ul/li[@class="active open"]/ul[@class="col-xs-12 dropdown mega-menu active"]/li[5]/a[1]/span[1]')
        return hl;
    },
    
    enquire:function(){
        var e = XPathSelector('//p[contains(text(),"Enquire about a new loan")]')
        return e;
    }
}