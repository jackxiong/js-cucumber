const Selector = require('testcafe').Selector
var xpath=''
const elementByXPath = Selector(xpath => {
    const iterator = document.evaluate(xpath, document, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null )
    const items = [];

    let item = iterator.iterateNext();

    while (item) {
        items.push(item);
        item = iterator.iterateNext();
    }

    return items;
});

module.exports = Selector(elementByXPath(xpath))
//export default function (xpath) {return module.exports = Selector(elementByXPath(xpath));}
//import { Selector } from 'testcafe';
//https://cucumber.io/docs/cucumber/api/