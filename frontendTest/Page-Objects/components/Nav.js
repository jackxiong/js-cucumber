import{Selector ,t } from 'testcafe'
import XPathSelector from '../../../xpath/xpath-selector'


class Nav{

    constructor(){

        this.personal = XPathSelector('//a[@class="menu-trigger"][contains(text(),"Personal")]');
        this.homeloan = XPathSelector('/html/body/div[1]/div/div[3]/div/header/div[3]/nav/ul/li[1]/ul/li[5]/div/ul/li[1]/a/span');
        this.hl       = XPathSelector('//body[@class="js"]/div[@class="outer"]/div[@id="main"]/div[@id="wrapper"]/div[@id="header-container"]/header/div[@id="menu-logo-container"]/nav[@class="hidden-xs primary-nav"]/ul/li[@class="active open"]/ul[@class="col-xs-12 dropdown mega-menu active"]/li[5]/a[1]/span[1]')
        this.enquire  = XPathSelector('//p[contains(text(),"Enquire about a new loan")]')
        this.title =    XPathSelector('//a[@id="logo"]//span[contains(text(),"NAB")]')
        this.login=     XPathSelector('//span[@class="mobile-login-label"]')
    }

    async goToEnquire (){

        
        await t.click(this.personal)
        await t.click(this.hl)
    
        try {

        await t.click(this.homeloan)
        await t.click(this.enquire)

        }catch (error){
          console.log("this is error")
          await t.wait(3000)
          await t.click(this.enquire)
        }


        
    }
}

export default Nav