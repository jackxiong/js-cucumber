const select = require('testcafe').Selector
const t = require('testcafe').t
const XPathSelector = require('../../../xpath/xpath-selector')

class searchPage {

    constructor(){

        this.searchKey   =       XPathSelector('//input[@name="q"]')
        this.searchbuton =       XPathSelector('//div[@class="FPdoLc tfB0Bf"]//input[@name="btnK"]')
        this.title       =       XPathSelector('//title[contains(text(),"Google")]')
    }
    async search(key){

        await t.typeText(this.searchKey,key)
        await t.click(this.searchbuton)

    }
    
}
//export default searchPage
module.exports = searchPage