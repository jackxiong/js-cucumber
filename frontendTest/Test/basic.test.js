import Nav from '../Page-Objects/components/Nav'
import searchPage from '../Page-Objects/pages/searchPage'

const  nav = new Nav()
const  g   = new searchPage()

fixture `Authentication tests`.page(`https://www.nab.com.au/`)
.before(async (t)=>{
       console.log('before test')
       //await t.setTestSpeed(0.1)
})
.beforeEach(async (t)=>{

       //await t.setTestSpeed(0.1)
})
.afterEach(async (t)=>{
        //await t.setTestSpeed(0.1)
})
.after(async (t)=>{

       console.log('after test')
})

test('nba home loan test ', async t => {

       await nav.goToEnquire()
       const text = await nav.personal.innerText
       await t.expect(text).eql("Personal")
       

})

test('google search test ', async t =>{
       
        var url = "http://www.google.com"
        await t.navigateTo(url)
        await t.expect(g.title.innerText).contains("Google")
        await g.search('Automation testing')
        await t.wait(3000)
                  
})