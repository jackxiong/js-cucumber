const https = require('https');
const assert = require('assert').strict;
//https://www.twilio.com/blog/2017/08/http-requests-in-node-js.html
var url ='https://api.weatherbit.io/v2.0/current?lat=40.730610&lon=-73.935242&Key=ba74ade333794824a918c4577f7634fd'
var url_='https://nodejs.org/dist/index.json'


function test_http(){

    https.get(url, (res) => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];
      
        let error;
        if (statusCode !== 200) {
          error = new Error('Request Failed.\n' +
                            `Status Code->: ${statusCode}`);
        } else if (!/^application\/json/.test(contentType)) {
          error = new Error('Invalid content-type.\n' +
                            `Expected application/json but received ${contentType}`);
        }
        if (error) {
          console.error(error.message);
          // Consume response data to free up memory
          res.resume();
          return;
        }
      
        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
          try {
            const parsedData = JSON.parse(rawData);
            console.log('=>',parsedData);
          } catch (e) {
            console.error(e.message);
          }
        });
      }).on('error', (e) => {
        console.error(`Got error: ${e.message}`);
      });

}
test_http()
